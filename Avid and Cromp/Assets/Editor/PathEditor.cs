using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class PathEditor : EditorWindow
{
	private static EditorWindow m_window;

	private string m_fileName 						= string.Empty;
	private bool m_bWillForceOverwrite 				= false;
	private bool[,] m_flags 						= new bool[5,3];
	private int[] m_sectionRows 					= new int[5];	
	
	private Vector2 OFFSET 							= new Vector2( 20.0f, 20.0f );

	[MenuItem( "Tools/Path Editor" )]
	public static void ShowWindow()
	{
		m_window = EditorWindow.GetWindow( typeof( PathEditor ), false, "Path Editor" );
		m_window.minSize = new Vector2( 300, 400 );
	}

	void OnGUI()
	{
		m_fileName = EditorGUI.TextField( new Rect( OFFSET.x, OFFSET.y, position.width - ( OFFSET.x * 2 ), 20.0f ), "File Name: ", m_fileName );
		
		EditorGUI.LabelField( new Rect( OFFSET.x, OFFSET.y + 30.0f, position.width - ( OFFSET.x * 2 ), 20.0f ), "Section Flags", EditorStyles.boldLabel );
		
		for( int i = 0; i < 5; i++ )
		{
			EditorGUI.LabelField( new Rect( OFFSET.x + ( i * 40.0f ), OFFSET.y + 55.0f, position.width - ( OFFSET.x * 2 ), 20.0f ), "S" + ( i + 1 ) );
			
			m_sectionRows[i] = 0;
			
			for( int j = 0; j < 3; j++ )
			{
				m_flags[i,j] = EditorGUI.Toggle( new Rect( OFFSET.x + ( i * 40.0f ), OFFSET.y + 75.0f + ( j * 25.0f ), 25.0f, 25.0f ), m_flags[i,j] );
				
				if( m_flags[i, j] )
				{
					m_sectionRows[i] += 1 << j;
				}
			}
		}
		
		m_bWillForceOverwrite = EditorGUI.Toggle( new Rect( OFFSET.x, OFFSET.y + 170.0f, position.width - ( OFFSET.x * 2 ), 20.0f ), "Overwrite if the file exists", m_bWillForceOverwrite );
		
		if( GUI.Button( new Rect( OFFSET.x, OFFSET.y + 195.0f, ( position.width / 2 ) - ( OFFSET.x + 10.0f ), 20.0f ), "Load" ) )
		{
			LoadData();
		}
		
		if( GUI.Button( new Rect( ( position.width / 2 ) + 10.0f, OFFSET.y + 195.0f, ( position.width / 2 ) - ( OFFSET.x + 10.0f ), 20.0f ), "Save" ) )
		{
			SaveData();
		}
	}

	void LoadData()
	{
		if( string.IsNullOrEmpty( m_fileName ) )
		{
			if( EditorUtility.DisplayDialog( "Warning", "You have to specify a file name.", "Okay" ) )
			{
				return;
			}
		}
		
		TextAsset textAsset = Resources.Load( "JSONData/Paths/" + m_fileName ) as TextAsset;
		if( textAsset == null )
		{
			if( EditorUtility.DisplayDialog( "Error", "Unable to find the file!", "Okay" ) )
			{
				return;
			}
		}
		
		PathData pathData = JsonMapper.ToObject<PathData>( textAsset.text );
		
		for( int i = 0; i < pathData.sectionFlags.Length; i++ )
		{
			for( int j = 0; j < 3; j++ )
			{
				//+LA 112015 TODO: Try to polish this
				if( ( pathData.sectionFlags[i] & ( 1 << j ) ) == 1 << j )
				{
					m_flags[i,j] = true;
				}
			}
		}
		
		Debug.Log( "File Loaded!" );
	}

	void SaveData()
	{
		if( string.IsNullOrEmpty( m_fileName ) )
		{
			if( EditorUtility.DisplayDialog( "Warning", "You have to specify a file name.", "Okay" ) )
			{
				return;
			}
		}
	
		if( m_fileName.EndsWith( ".txt" ) )
		{
			m_fileName = m_fileName.Substring( 0, m_fileName.Length - 4 ); //This will remove .txt
		}
	
		string fileLocation = string.Format( "{0}/Resources/JSONData/Paths/{1}.txt", Application.dataPath, m_fileName );
		
		if( !m_bWillForceOverwrite && System.IO.File.Exists( fileLocation ) )
		{
			if( !EditorUtility.DisplayDialog( "Warning", "File already exists. Do you want to overwrite?", "Yes", "Cancel" ) )
			{
				return;
			}
		}
		
		PathData pathData = new PathData();
		pathData.sectionFlags = m_sectionRows;
		
		try
		{
			System.IO.StreamWriter writer = new System.IO.StreamWriter( fileLocation );
			string jsonData = JsonMapper.ToJson( pathData );
			writer.WriteLine( jsonData );
			writer.Close();
			AssetDatabase.Refresh();
			Debug.Log( "File Saved!" );
		}
		catch( UnityException ex )
		{
			Debug.LogError( ex.Message );
		}
	}
}
