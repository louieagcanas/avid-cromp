﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	private Transform m_transform;
	private Transform m_mainCameraTransform;
	private Transform m_playerTransform;

	void Awake()
	{
		m_transform = transform;
		m_mainCameraTransform = Camera.main.transform;
	}
	
	void Update () 
	{
		if( m_playerTransform != null )
		{
			//+LA 110815 TODO: Improve the lerping effect
			m_transform.position = new Vector3( Mathf.Lerp( m_transform.position.x, m_playerTransform.position.x, 0.05f ), 
															m_transform.position.y, 
															m_transform.position.z );
		}
	}
	
	public void ControlMainCamera()
	{
		m_mainCameraTransform.parent = m_transform;
	}
	
	public void DetachMainCamera()
	{
		m_mainCameraTransform.parent = null;
		m_mainCameraTransform.position = Vector3.zero;
	}
	
	public void Follow( Transform p_playerTransform )
	{
		m_playerTransform = p_playerTransform;
	}
	
	public void StopFollow()
	{
		m_playerTransform = null;
	}
}
