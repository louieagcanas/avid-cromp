using UnityEngine;
using System.Collections;

public class MonsterController : MonoBehaviour 
{
	private Transform m_transform;
	private Transform m_playerTransform;

	private float m_movementSpeed = 1.5f;
	
	void Awake()
	{
		m_transform = transform;
	}
	
	void Start()
	{
		//Temp
		GetComponent<Renderer>().material.color = Color.black;
	}
	
	void Update()
	{
		if( m_playerTransform != null ) 
		{
			//+LA 110815 TODO: Find a better way to handle the speed
			m_movementSpeed = ( m_playerTransform.position.x - m_transform.position.x < 5.0f ) ? 1.5f : 2.5f;
			
			Vector3 distance = new Vector3( m_playerTransform.position.x, 0.0f, 0.0f ) - new Vector3( m_transform.position.x, 0.0f, 0.0f );
			m_transform.position += distance.normalized * m_movementSpeed * Time.deltaTime;
			
			if( m_playerTransform.position.x - m_transform.position.x <= 0.01f )
			{
				m_transform.position = new Vector3( m_playerTransform.position.x, m_transform.position.y, m_transform.position.z );
				GameManager.Instance.GameOver();
			}
		}
	}
	
	public void Chase( Transform p_playerTransform )
	{
		m_playerTransform = p_playerTransform;
	}
	
	public void StopChase()
	{
		m_playerTransform = null;
	}
}
