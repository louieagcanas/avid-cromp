using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	private Transform m_transform;
	private Rigidbody m_rigidbody;
	
	public LayerMask m_groundLayer;
	public Transform m_groundCheck;
	private float m_groundCheckRadius 	= 0.1f;
	private bool m_bIsGrounded 			= false;
	
	private float m_jumpForce 			= 500.0f;
	private float m_targetPosition 		= 0.0f;
	
	void Awake()
	{
		m_transform = transform;
		m_rigidbody = GetComponent<Rigidbody>();
	}

	void Start()
	{
		m_targetPosition = m_transform.position.x;
	}
	
	void LateUpdate()
	{
		m_bIsGrounded = Physics.CheckSphere( m_groundCheck.position, m_groundCheckRadius, m_groundLayer );
	}

	void Update()
	{
		if( GameManager.Instance.CurrentState != GameManager.GameState.Game ) return;
	
		m_transform.position = new Vector3( Mathf.Lerp( m_transform.position.x, m_targetPosition, 0.25f ), m_transform.position.y, m_transform.position.z );

		//+LA 110415 TODO: Polish this
		if( Physics.Raycast( m_transform.position - new Vector3( 0.0f, ( m_transform.localScale.y / 2.0f ) - 0.05f, 0.0f ), Vector3.right, 0.1f ) ) 
		{
			Collided();
		}
	}
	
	private void Collided()
	{
		//+LA 102715 TODO: Add camera shake
		ProgressManager.Instance.IsInPenalty = true;
		MoveBack();
	}
	
	private void MoveBack()
	{
		m_targetPosition -= 1.2f;
	}
	
	public void MoveForward()
	{
		m_targetPosition += 1.2f;
	}

	public void Jump()
	{
		if( m_bIsGrounded )
		{
			m_rigidbody.AddForce( Vector2.up * m_jumpForce );
		}
	}
	
	public void Reset()
	{
		m_targetPosition = 0.0f;
	}
}
