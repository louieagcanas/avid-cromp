﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathSorter : MonoBehaviour 
{
	private static float m_pathTotalOffset	= 0.0f;
	
	private float m_pathOffset				= 0.5f;
	private float m_sectionOffset 			= 0.1f;
	
	private Transform m_transform;
	private List<Transform> m_sections = new List<Transform>();

	void Awake()
	{
		m_transform = transform;
		
		for( int i = 0; i < m_transform.childCount; i++ )
		{
			m_sections.Add( m_transform.GetChild( i ).transform );
		}
	}

	public void Organize()
	{
		m_transform.position = new Vector3( m_transform.position.x, m_transform.position.y, m_pathTotalOffset );
		m_pathTotalOffset -= m_pathOffset;
		
		for( int i = 0; i < m_sections.Count; i++ )
		{
			m_sections[i].localPosition = new Vector3( m_sections[i].localPosition.x, m_sections[i].localPosition.y, i * -m_sectionOffset );
		}
	}
}
