﻿using UnityEngine;
using System.Collections;

public class MainController : MonoBehaviour 
{
	#region Enum
	public enum State
	{
		None = -1,
		Initialization,
		MainMenu,
		Gameplay,
	}
	#endregion
	
	#region Methods
	void Start()
	{
		SetState( State.Initialization );
	}

	public static void SetState( State p_state )
	{
		switch( p_state )
		{
		case State.Initialization:
			Block.LoadTextures();
			Symbol.LoadTextures();
		
			DataManager.LoadData(); 
			PathManager.Instance.Initialize();
			GameManager.Instance.Initialize();
			ScreensManager.Instance.Initialize();
			SetState( State.MainMenu );
			break;

		case State.MainMenu:
			ScreensManager.Instance.SetUIScreen( ScreensManager.Screens.MainMenu );
			break;

		case State.Gameplay:
			GameManager.Instance.StartGame();
			break;
		}
	}
	#endregion
}
