using UnityEngine;
using System.Collections;

public class ProgressManager : MonoBehaviour 
{
	private static ProgressManager m_instance;
	public static ProgressManager Instance
	{
		get
		{
			if( m_instance == null )
			{
				m_instance = GameObject.FindObjectOfType<ProgressManager>();

				if( m_instance == null )
				{
					GameObject container = new GameObject( "Progress Manager" );
					m_instance = container.AddComponent<ProgressManager>();
				}
			}

			return m_instance;
		}
	}

	private int m_progress 		= 0;
	private bool m_bIsInPenalty = false;

	public int GetProgress()
	{
		return ( m_bIsInPenalty ) ? m_progress - 1 : m_progress;
	}

	public bool IsInPenalty
	{
		get{ return m_bIsInPenalty; }
		set{ m_bIsInPenalty = value; }
	}

	public void TryToUpdateProgress()
	{
		if( !m_bIsInPenalty )
		{
			Debug.Log( "Not in penalty!" );
			m_progress++;
			//+LA 110415 TODO: Update score

			//+LA 110415 TODO: Polish this
			if( m_progress >= 15 )
			{
				if( m_progress % 5 == 0 )
				{
					
				}
			}
			//-LA
		}
		else
		{
			Debug.Log( "In Penalty!" );
		}
		
		m_bIsInPenalty = false;
	}
	
	public void Reset()
	{
		m_progress = 0;
	}
}
