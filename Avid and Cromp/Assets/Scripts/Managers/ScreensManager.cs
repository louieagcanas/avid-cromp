﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScreensManager : MonoBehaviour 
{
	private static ScreensManager m_instance;
	public static ScreensManager Instance
	{
		get
		{
			if( m_instance == null )
			{
				m_instance = GameObject.FindObjectOfType<ScreensManager>();

				if( m_instance == null )
				{
					GameObject container = new GameObject( "ScreensManager" );
					m_instance = container.AddComponent<ScreensManager>();
				}
			}

			return m_instance;
		}
	}

	public enum Screens
	{
		None = -1,
		MainMenu,
		Instructions,
		Gameplay,
		GameOver,
		Options,
		Credits,
		MAX
	}

	private string[] m_screenKey = 
	{
		"MainMenu",
		"Instructions",
		"Gameplay",
		"GameOver",
		"Options",
		"Credits"
	};

	private Transform m_transform;
	private List<GameObject> m_screens = new List<GameObject>();

	void Awake()
	{
		m_transform = transform;
	}

	public void Initialize()
	{
		//Load all screen as prefab
		for( int i = 0; i < (int)Screens.MAX; i++ )
		{
			GameObject screenPrefab = Resources.Load<GameObject>( Constants.UI_SCREENS_PATH + m_screenKey[i] );
			if( screenPrefab == null ) continue; //To prevent using null prefab
			GameObject screen = Instantiate( screenPrefab, Vector3.zero, Quaternion.identity ) as GameObject;
			screen.transform.SetParent( m_transform );
			screen.SetActive( false );
			m_screens.Add( screen );
		}
	}

	public void SetUIScreen( Screens p_screen )
	{
		int screenIndex = (int)p_screen;

		if( screenIndex >= m_screens.Count ) return;
		
		for( int i = 0; i < m_screens.Count; i++ )
		{
			if( i == screenIndex )
			{
				ShowScreen( i );
			}
			else
			{
				HideScreen( i );
			}
		}
	}

	private void ShowScreen( int p_screenIndex )
	{
		m_screens[p_screenIndex].SetActive( true );
	}

	private void HideScreen( int p_screenIndex )
	{
		m_screens[p_screenIndex].SetActive( false );
	}
}
