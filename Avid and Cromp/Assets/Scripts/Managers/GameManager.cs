﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	#region Singleton
	private static GameManager m_instance;
	public static GameManager Instance
	{
		get
		{
			if( m_instance == null )
			{
				m_instance = GameObject.FindObjectOfType<GameManager>();

				if( m_instance == null )
				{
					GameObject container = new GameObject( "Game Manager" );
					m_instance = container.AddComponent<GameManager>();
				}
			}

			return m_instance;
		}
	}
	#endregion
	
	#region Enum
	public enum GameState
	{
		None = -1,
		Instructions,
		Game,
		GameOver	
	}
	#endregion
	
	#region Variables
	private GameState m_currentState = GameState.None; //+LA 112215 TODO: Rename this
	
	private PlayerController m_player;
	private MonsterController m_monster;
	private CameraController m_cameraController;
	
	private Vector3 m_playerStartPosition 	= new Vector3( 0.0f, 2.0f, 0.0f );
	private Vector3 m_monsterStartPosition 	= new Vector3( -6.0f, 0.0f, 0.0f );
	#endregion
	
	#region Properties
	public GameState CurrentState //+LA 112215 TODO: Rename this
	{
		get{ return m_currentState; }
	}
	#endregion
	
	#region Methods
	void Update()
	{
		if( Input.GetMouseButtonDown( 0 ) )
		{
			if( m_currentState == GameState.Instructions )
			{
				SetState( GameState.Game );
			}
		}
	}
	
	public void SetState( GameState p_state )
	{
		switch( p_state )
		{
		case GameState.Instructions:
			ScreensManager.Instance.SetUIScreen( ScreensManager.Screens.Instructions );
			break;
		case GameState.Game:
			ScreensManager.Instance.SetUIScreen( ScreensManager.Screens.Gameplay );
			InputManager.Instance.Enable();
			
			//+LA 112215 TODO: Try to polish this
			m_cameraController.Follow( m_player.transform );
			m_monster.Chase( m_player.transform );
			break;
		case GameState.GameOver:
			ScreensManager.Instance.SetUIScreen( ScreensManager.Screens.GameOver );
			InputManager.Instance.Disable();
			
			//+LA 112215 TODO: Try to polish this
			m_cameraController.StopFollow();
			m_monster.StopChase();
			break;
		}
		
		m_currentState = p_state;
	}

	public void Initialize()
	{
		//Initialize player
		GameObject playerPrefab = Resources.Load<GameObject>( Constants.CHARACTERS_PATH + "Player" );
		GameObject player = Instantiate( playerPrefab, Vector3.zero, Quaternion.identity ) as GameObject;
		player.SetActive( false );
		m_player = player.GetComponent<PlayerController>();
		
		//Initialize monster
		GameObject monsterPrefab = Resources.Load<GameObject>( Constants.CHARACTERS_PATH + "Monster" );
		GameObject monster = Instantiate( monsterPrefab, Vector3.zero, Quaternion.identity ) as GameObject;
		monster.SetActive( false );
		m_monster = monster.GetComponent<MonsterController>();
		
		//Initialize camera
		GameObject cameraControllerPrefab = Resources.Load<GameObject>( Constants.OBJECTS_PATH + "CameraController" );
		GameObject cameraController = Instantiate( cameraControllerPrefab, Vector3.zero, Quaternion.identity ) as GameObject;
		m_cameraController = cameraController.GetComponent<CameraController>();
	}
	
	public void StartGame()
	{
		InitialSetup();
		SetState( GameState.Instructions );
	}
	
	private void InitialSetup()
	{
		m_player.transform.position = m_playerStartPosition;
		m_player.gameObject.SetActive( true );
		
		m_monster.transform.position = m_monsterStartPosition;
		m_monster.gameObject.SetActive( true );
		
		m_cameraController.ControlMainCamera();
		
		//+LA 110815 TODO: Double check these method calls
		PathManager.Instance.InitialSetup();
		//InputManager.Instance.SetPlayer( m_player );
	}

	public void PlayerMove()
	{
		m_player.MoveForward();
	}

	public void PlayerJump()
	{
		m_player.Jump();
	}
	
	public void GameOver()
	{
		SetState( GameState.GameOver );
	}
	
	public void Retry()
	{
		ProgressManager.Instance.Reset();
		PathManager.Instance.Reset();
		m_player.Reset();
		
		m_player.transform.position = m_playerStartPosition;
		m_monster.transform.position = m_monsterStartPosition;
		m_cameraController.transform.position = Vector3.zero;
		
		SetState( GameState.Instructions );
	}
	
	public void BackToMainMenu()
	{
		
	}
	#endregion
}
