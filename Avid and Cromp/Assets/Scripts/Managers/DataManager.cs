﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class DataManager : MonoBehaviour 
{
	#region Variables
	private static List<PathData> m_pathDataList = new List<PathData>();
	#endregion
	
	#region Properties
	public static List<PathData> PathDataList
	{
		get{ return m_pathDataList; }
	}
	#endregion
	
	#region Methods
	public static void LoadData() //+LA 112215: In case we have other stuff to load
	{
		LoadPathData();
	}
	
	private static void LoadPathData()
	{
		TextAsset[] textAssets = Resources.LoadAll<TextAsset>( Constants.PATH_DATA_PATH );
		
		for( int i = 0; i < textAssets.Length; i++ )
		{
			PathData pathData = JsonMapper.ToObject<PathData>( textAssets[i].text );
			m_pathDataList.Add( pathData );
		}
	}
	#endregion
}
