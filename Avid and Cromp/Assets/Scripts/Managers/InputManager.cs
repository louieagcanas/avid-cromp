﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour 
{
	#region Singleton
	private static InputManager m_instance;
	public static InputManager Instance
	{
		get
		{
			if( m_instance == null )
			{
				m_instance = GameObject.FindObjectOfType<InputManager>();
				
				if( m_instance == null )
				{
					GameObject container = new GameObject( "Input Manager" );
					m_instance = container.AddComponent<InputManager>();
				}
			}
		
			return m_instance;
		}
	}
	#endregion

	#region Variables
	private bool m_bIsEnabled = false;
	#endregion

	#region Methods
	void Update() 
	{
		if( !m_bIsEnabled ) return; 
	
		if( Input.GetKeyDown( KeyCode.D ) )
		{
			if( InputChecker.IsInputCorrect( Constants.SymbolInput.Red ) )
			{
				GameManager.Instance.PlayerMove();
			}
		}
		else if( Input.GetKeyDown( KeyCode.F ) )
		{
			if( InputChecker.IsInputCorrect( Constants.SymbolInput.Blue ) )
			{
				GameManager.Instance.PlayerMove();
			}
		}
		else if( Input.GetKeyDown( KeyCode.J ) )
		{
			if( InputChecker.IsInputCorrect( Constants.SymbolInput.Yellow ) )
			{
				GameManager.Instance.PlayerMove();
			}
		}
		else if( Input.GetKeyDown( KeyCode.K ) )
		{
			if( InputChecker.IsInputCorrect( Constants.SymbolInput.Green ) )
			{
				GameManager.Instance.PlayerMove();
			}
		}
		else if( Input.GetKeyDown( KeyCode.Space ) )
		{
			GameManager.Instance.PlayerJump();
		}
	}
	
	public void Enable()
	{
		m_bIsEnabled = true;
	}
	
	public void Disable()
	{
		m_bIsEnabled = false;
	}
	#endregion
}
