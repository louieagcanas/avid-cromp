﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputChecker : MonoBehaviour 
{
	#region Variables
	private static List<Constants.SymbolInput> m_inputs = new List<Constants.SymbolInput>();
	#endregion
	
	#region Methods
	public static void RegisterInput( Constants.SymbolInput p_input )
	{
		m_inputs.Add( p_input );
	}
	
	public static bool IsInputCorrect( Constants.SymbolInput p_input )
	{
		//+LA 11222015 TODO: Try to find a better way to implement this
		if( m_inputs[ProgressManager.Instance.GetProgress()] == p_input )
		{
			return true;
		}
		
		return false;
	}
	#endregion
}
