﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathManager : MonoBehaviour 
{
	#region Singleton
	private static PathManager m_instance;
	public static PathManager Instance
	{
		get
		{
			if( m_instance == null )
			{
				m_instance = GameObject.FindObjectOfType<PathManager>();
			
				if( m_instance == null )
				{
					GameObject container = new GameObject( "Path Manager" );
					m_instance = container.AddComponent<PathManager>();
				}
			}

			return m_instance;
		}
	}
	#endregion

	#region Variables
	private List<Path> m_pathPool 		= new List<Path>();
	private const int POOL_AMOUNT 		= 5;
	
	private float m_pathOffset			= 0.0f;
	private float m_nextPathPosition 	= 1.2f; //+LA 112215 TODO: Double check this
	#endregion

	#region Methods
	public void Initialize()
	{
		m_pathOffset = Constants.BLOCK_SIZE * 5;
	
		GameObject pathPrefab = Resources.Load<GameObject>( Constants.OBJECTS_PATH + "Base Path" ) as GameObject; 

		for( int i = 0; i < POOL_AMOUNT; i++ )
		{
			GameObject pathObject = Instantiate( pathPrefab, Vector3.zero, Quaternion.identity ) as GameObject;
			pathObject.transform.parent = transform;
			
			Path path = pathObject.GetComponent<Path>();
			path.Deactivate();
			
			m_pathPool.Add( path );
		}
	}
	
	public void InitialSetup()
	{
		//+LA 112315 TODO: Double check this
		for( int i = 0; i < 5; i++ )
		{
			CreatePath();
		}
	}
	
	public void CreatePath()
	{
		Path path = GetAvailablePath();
		
		PathData pathData = DataManager.PathDataList[Random.Range( 0, DataManager.PathDataList.Count )];
		
		path.SetPosition( new Vector3( m_nextPathPosition, Constants.PLATFORM_BASE_POSITION ) );
		path.Build( pathData.sectionFlags );
		path.Activate();
		
		m_nextPathPosition += m_pathOffset;
	}
	
	public void Reset()
	{
		m_nextPathPosition = 1.2f; //+LA 112215 TODO: Double check this
		
		//+LA 112215 TODO: Double check this
		for( int i = 0; i < m_pathPool.Count; i++ )
		{
			if( m_pathPool[i].IsActive() )
			{
				m_pathPool[i].Deactivate();
			}
		}
		
		InitialSetup();
	}
	
	private Path GetAvailablePath()
	{
		for( int i = 0; i < m_pathPool.Count; i++ )
		{
			if( !m_pathPool[i].IsActive() )
			{
				return m_pathPool[i];
			}
		}
		
		return null;
	}
	#endregion
}
