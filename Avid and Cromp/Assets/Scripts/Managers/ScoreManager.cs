﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour 
{
	private static ScoreManager m_instance;

	public static ScoreManager Instance
	{
		get
		{
			if( m_instance == null )
			{
				m_instance = GameObject.FindObjectOfType<ScoreManager>();

				if( m_instance == null )
				{
					GameObject container = new GameObject( "Score Manager" );
					m_instance = container.AddComponent<ScoreManager>();
				}
			}

			return m_instance;
		}
	}

	private int m_score = 0;
	private int m_combo = 0;

	public int Score
	{
		get{ return m_score; }
	}

	public int Combo
	{
		get{ return m_combo; }
	}

	public void AddScore()
	{
		m_score++;
		m_combo++;
	}

	public void DeductScore()
	{
		if( m_score > 0 )
		{
			m_score--;
		}

		m_combo = 0; //Reset
	}
}
