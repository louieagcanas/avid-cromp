﻿using UnityEngine;
using System.Collections;

public class GameOverScreen : MonoBehaviour 
{
	public void Retry()
	{
		GameManager.Instance.Retry();
	}

	public void BackToMainMenu()
	{
		GameManager.Instance.BackToMainMenu();
	}
}
