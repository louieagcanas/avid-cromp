﻿using UnityEngine;
using System.Collections;

public class MainMenuScreen : MonoBehaviour 
{
	public void Play()
	{
		MainController.SetState( MainController.State.Gameplay );
	}

	public void Options()
	{

	}

	public void Credits()
	{

	}
}
