﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Symbol : MonoBehaviour 
{
	private static List<Sprite> m_textures = new List<Sprite>();

	private SpriteRenderer m_spriteRenderer;
	
	void Awake()
	{
		m_spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public static void LoadTextures()
	{
		//We did this because the order of the symbols is improtant
		Sprite texture = Resources.Load<Sprite>( Constants.SYMBOL_TEXTURES_PATH + "Red Symbol" );
		m_textures.Add( texture );
		
		texture = Resources.Load<Sprite>( Constants.SYMBOL_TEXTURES_PATH + "Blue Symbol" );
		m_textures.Add( texture );
		
		texture = Resources.Load<Sprite>( Constants.SYMBOL_TEXTURES_PATH + "Yellow Symbol" );
		m_textures.Add( texture );
		
		texture = Resources.Load<Sprite>( Constants.SYMBOL_TEXTURES_PATH + "Green Symbol" );
		m_textures.Add( texture );
	
		Debug.Log( "Textures: " + m_textures.Count );	
	}
	
	public void Activate()
	{
		gameObject.SetActive( true );
		
		int randomNumber = Random.Range( 0, (int)Constants.SymbolInput.MAX );
		
		Constants.SymbolInput symbolInput = (Constants.SymbolInput)randomNumber;
		
		InputChecker.RegisterInput( symbolInput );
		
		m_spriteRenderer.sprite = m_textures[randomNumber];
	}
	
	public void Deactivate()
	{
		gameObject.SetActive( true );
	}
}
