﻿using UnityEngine;
using System;
using System.Collections;

public class Constants
{
	[Flags]
	public enum SectionFlag
	{
		Top 	= 1 << 0,
		Middle 	= 1 << 1,
		Bottom 	= 1 << 2,
	}
	
	public enum SymbolInput
	{
		None = -1,
		Red,
		Blue,
		Yellow,
		Green,
		MAX
	}

	//For data
	public static string PATH_DATA_PATH 		= "JSONData/Paths/";
	
	//For prefabs
	public static string UI_SCREENS_PATH 		= "Prefabs/UI/Screens/";
	public static string CHARACTERS_PATH 		= "Prefabs/Characters/";
	public static string OBJECTS_PATH 			= "Prefabs/Objects/";

	//For textures
	public static string BLOCK_TEXTURES_PATH 	= "Textures/Blocks/";
	public static string SYMBOL_TEXTURES_PATH 	= "Textures/Symbols/";
	
	public static float BLOCK_SIZE 				= 1.2f;
	public static float PLATFORM_BASE_POSITION 	= -1.75f;
}
