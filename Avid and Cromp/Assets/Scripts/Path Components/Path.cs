﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Path : MonoBehaviour 
{
	#region Variables
	private Transform m_transform;

	private PathSorter m_pathSorter;
	private List<Section> m_sections = new List<Section>();
	#endregion

	#region Methods
	void Awake()
	{
		m_transform = transform;

		m_pathSorter = GetComponent<PathSorter>();
	
		for( int i = 0; i < m_transform.childCount; i++ )
		{
			Section section = m_transform.GetChild( i ).GetComponent<Section>();
			m_sections.Add( section );
		}
	}
	
	//+LA 112215 TODO: Add an overload method for path with hazards
	public void Build( int[] p_flags )
	{
		m_pathSorter.Organize();

		for( int i = 0; i < m_sections.Count; i++ )
		{
			Constants.SectionFlag flag = (Constants.SectionFlag)p_flags[i];
			m_sections[i].ActivateBlocks( flag );
			m_sections[i].ActivateSymbol();
		}
	}

	public void SetPosition( Vector3 p_position )
	{
		m_transform.position = p_position;
	}
	
	public bool IsActive()
	{
		return gameObject.activeInHierarchy; 
	}

	public void Activate()
	{
		gameObject.SetActive( true );
	}

	public void Deactivate()
	{
		gameObject.SetActive( false );
	}
	#endregion
}
