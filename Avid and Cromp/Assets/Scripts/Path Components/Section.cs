﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Section : MonoBehaviour
{
	#region Variables
	private Transform m_transform;
	
	private List<Block> m_blocks = new List<Block>();
	private Symbol m_symbol;
	#endregion
	
	#region Methods
	public void Awake()
	{
		m_transform = transform;
		
		Transform blocksParent = m_transform.FindChild( "Blocks" );
		
		for( int i = 0; i < blocksParent.childCount; i++ )
		{
			Block block = blocksParent.GetChild(i).GetComponent<Block>();
			m_blocks.Add( block );
		}
		
		m_symbol = m_transform.FindChild( "Symbol" ).GetComponent<Symbol>();
		m_symbol.Deactivate();
	}

	public void ActivateBlocks( Constants.SectionFlag p_flag )
	{
		if( ( p_flag & Constants.SectionFlag.Top ) == Constants.SectionFlag.Top )
		{
			m_blocks[0].Activate();
		}
		//+LA 112315 TODO: Double check this, this is for when we are recycling a path to make sure to deactivate a previously active block
		else
		{
			m_blocks[0].Deactivate();
		}
		
		if( ( p_flag & Constants.SectionFlag.Middle ) == Constants.SectionFlag.Middle )
		{
			m_blocks[1].Activate();
		}
		//+LA 112315 TODO: Double check this, this is for when we are recycling a path to make sure to deactivate a previously active block
		else
		{
			m_blocks[1].Deactivate();
		}
		
		if( ( p_flag & Constants.SectionFlag.Bottom ) == Constants.SectionFlag.Bottom )
		{
			m_blocks[2].Activate();
		}
		//+LA 112315 TODO: Double check this, this is for when we are recycling a path to make sure to deactivate a previously active block
		else
		{
			m_blocks[2].Deactivate();
		}
	}
	
	public void ActivateSymbol()
	{
		m_symbol.Activate();
	}
	#endregion
}
