﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour
{
	private static Sprite[] m_textures;

	private SpriteRenderer m_spriteRenderer;
	
	void Awake()
	{
		m_spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
	}

	public static void LoadTextures()
	{
		m_textures = Resources.LoadAll<Sprite>( Constants.BLOCK_TEXTURES_PATH );
	}
	
	public void Activate()
	{
		gameObject.SetActive( true );
		
		m_spriteRenderer.sprite = m_textures[Random.Range( 0, m_textures.Length )];
	}
	
	public void Deactivate()
	{
		gameObject.SetActive( false );
	}
}
